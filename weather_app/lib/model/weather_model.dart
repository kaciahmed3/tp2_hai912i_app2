class WeatherModel{
   dynamic _temp;
   dynamic _pressure;
   dynamic _humidity;
   dynamic _weatherDescription;
   dynamic _tempMax;
   dynamic _tempMin;


   WeatherModel(this._temp, this._pressure, this._humidity,
      this._weatherDescription, this._tempMax, this._tempMin);


   dynamic get temp => _temp;

  set temp(dynamic value) {
    _temp = value;
  }



   dynamic get pressure => _pressure;

  set pressure(dynamic value) {
    _pressure = value;
  }

   dynamic get humidity => _humidity;

  set humidity(dynamic value) {
    _humidity = value;
  }

   dynamic get weatherDescription => _weatherDescription;

  set weatherDescription(dynamic value) {
    _weatherDescription = value;
  }

   dynamic get tempMax => _tempMax;

  set tempMax(dynamic value) {
    _tempMax = value;
  }

   dynamic get tempMin => _tempMin;

  set tempMin(dynamic value) {
    _tempMin = value;
  }

   factory WeatherModel.fromJson(final jsonDecoded) {
     print("***************");
     print(jsonDecoded["weather"].runtimeType);
     Map<String,dynamic>jsonWeather =jsonDecoded["main"];
     List<dynamic> jsonWeatherDesc = jsonDecoded["weather"];
     return WeatherModel(jsonWeather["temp"], jsonWeather["pressure"], jsonWeather["humidity"], jsonWeatherDesc.first["description"], jsonWeather["temp_max"], jsonWeather["temp_min"]);
   }
}