part of 'weather_dart_bloc.dart';

@immutable
abstract class WeatherDartState extends Equatable {
  @override
  List<Object> get props => [];
}

class WeatherDartInitial extends WeatherDartState {}

class WeatherDartIsNotSearched extends WeatherDartState{}
class WeatherDartIsNotLoaded extends WeatherDartState{}
class WeatherDartIsLoading extends WeatherDartState{}

class WeatherDartIsLoaded extends WeatherDartState{

  final WeatherModel _weather;

  WeatherDartIsLoaded(this._weather);

  WeatherModel get getWeather => _weather;

  @override
  List<Object> get props => [_weather];
}