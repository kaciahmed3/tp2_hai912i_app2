part of 'weather_dart_bloc.dart';

@immutable
abstract class WeatherDartEvent extends Equatable
{
  @override
  List<Object> get props => [];
}
class FetchDartWeather extends WeatherDartEvent{
  final _city;

  FetchDartWeather(this._city);

  @override
  List<Object> get props => [_city];
}

class ResetDartWeather extends WeatherDartEvent{

}