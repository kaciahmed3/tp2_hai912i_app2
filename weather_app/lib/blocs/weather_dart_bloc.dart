import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:weather_app/model/weather_model.dart';
import 'package:weather_app/repositories/weather_repo.dart';

part 'weather_dart_event.dart';
part 'weather_dart_state.dart';

class WeatherDartBloc extends Bloc<WeatherDartEvent, WeatherDartState> {
  Network weatherRepo;

  WeatherDartBloc(this.weatherRepo): super(WeatherDartInitial());

  // no weather is reached at the begining
  @override
  WeatherDartState get initialState => WeatherDartInitial();

  @override
  Stream<WeatherDartState> mapEventToState(WeatherDartEvent event) async*{

    if(event is FetchDartWeather) {
      WeatherModel weather = await weatherRepo.getWeatherForcast(
          cityName: event._city);
      try {
        yield WeatherDartIsLoaded(weather);
      }catch(_){
        print("Error on loading weather");
        yield WeatherDartIsNotLoaded();
      }
    }else if(event is ResetDartWeather){
      yield WeatherDartIsNotSearched();
    }


  }
}
