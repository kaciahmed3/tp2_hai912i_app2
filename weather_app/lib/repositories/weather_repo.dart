import 'package:http/http.dart';
import 'dart:convert';

import 'package:weather_app/model/weather_model.dart';

class Network{
  Future<WeatherModel> getWeatherForcast({required String cityName}) async{
    var finalUrl ="https://api.openweathermap.org/data/2.5/weather?q=$cityName&appid=45cba5ad7bb9d2dc0a1d5ff132b1d8d6";
    final response = await get(Uri.parse(finalUrl));
    print("URL : ${Uri.encodeFull(finalUrl)}");

    if(response.statusCode == 200){
      print("weather data: ${response.body}");
      return WeatherModel.fromJson(json.decode(response.body));
    }
      throw Exception("Error getting weather forecast");

  }
}