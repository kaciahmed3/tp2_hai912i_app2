import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:weather_app/blocs/weather_dart_bloc.dart';
import 'package:weather_app/repositories/weather_repo.dart';

import 'model/weather_model.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
      ),
      home: Scaffold(
        backgroundColor: Colors.white,
        body:  BlocProvider(
          create: (context)=> WeatherDartBloc(Network()),
          child:  const SafeArea(
              child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.0),
              child: MyHomePage( title: 'Météo'),
              ),
          )
        )

      )
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  Widget build(BuildContext context) {
    final weatherBloc = BlocProvider.of<WeatherDartBloc>(context);
    var cityController = TextEditingController();
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Center(
            child: Container(
                padding: EdgeInsets.only(right: 32, left: 32, top: 60),
                child: Column(
                  children: const <Widget>[
                  Text("Météo en Temps Réel",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 30,
                      fontWeight: FontWeight.bold),
                  ),
                SizedBox(height: 30),
                    FaIcon(
                      FontAwesomeIcons.cloudSunRain,
                      color: Colors.redAccent,
                      size: 200,
                    )
                ]
            )
            )
        ),

        BlocBuilder<WeatherDartBloc, WeatherDartState>(
        builder: (context, state){
           if(state is WeatherDartIsNotSearched || state is WeatherDartInitial)
              return Container(
                padding: EdgeInsets.only(top: 32, left: 32, right: 32,),
                child: Column(
                  children: <Widget>[
                    Text(
                      "Aujourd'hui :  ${new DateTime.now().day} /${new DateTime.now().month} /${new DateTime.now().year}",
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w500,
                          color: Colors.black),
                    ),
                    SizedBox(
                      height: 24,
                    ),
                    TextFormField(
                      controller: cityController,
                      decoration: InputDecoration(
                        prefixIcon: Icon(
                          Icons.search,
                          color: Colors.black,
                        ),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            borderSide: BorderSide(
                                color: Colors.black, style: BorderStyle.solid)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            borderSide: BorderSide(
                                color: Colors.redAccent, style: BorderStyle.solid)),
                        hintText: "Insérez le nom d'une ville",
                        hintStyle: TextStyle(color: Colors.grey[700]),
                      ),
                      style: TextStyle(color: Colors.black),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      width: double.infinity,
                      height: 50,
                      child: FlatButton(
                        shape: new RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(10))),
                        onPressed: () {
                          weatherBloc.add(FetchDartWeather(cityController.text));
                        },
                        color: Colors.red,
                        child: Text(
                          "Recherche",
                          style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),
                        ),
                      ),
                    )
                  ],
                ),
              );
           else if(state is WeatherDartIsLoading)
             return Center(child : CircularProgressIndicator());
           else if(state is WeatherDartIsLoaded)
             return ShowWeather(state.getWeather, cityController.text);
           else
             return Text("Error",style: TextStyle(color: Colors.white),);
         },
        )
      ],
    );
  }
}

class ShowWeather extends StatelessWidget {
  WeatherModel weather;
  final city;

  ShowWeather(this.weather, this.city);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.only(right: 32, left: 32),
        child: Column(
          children: <Widget>[
            Text(city,style: TextStyle(color: Colors.black, fontSize: 30, fontWeight: FontWeight.bold),),
            SizedBox(height: 10,),
            Text(weather.temp.toString()+" °F",style: TextStyle(color: Colors.black, fontSize: 50),),
            Text("Température",style: TextStyle(color: Colors.black, fontSize: 14,fontWeight: FontWeight.bold),),
            // icon which change according to the temperature Description
            Builder(
              builder: (context) {
                return getWeatherIcon(weatherDescription: weather.weatherDescription,color: Colors.orangeAccent, size: 30);
              }
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Text(weather.tempMin.toString()+" °F",style: TextStyle(color: Colors.black, fontSize: 20),),
                    Text("Min Température",style: TextStyle(color: Colors.black, fontSize: 14,fontWeight: FontWeight.bold),),
                  ],
                ),
                Column(
                  children: <Widget>[
                    Text(weather.tempMax.round().toString()+" °F",style: TextStyle(color: Colors.black, fontSize: 20),),
                    Text("Max Température",style: TextStyle(color: Colors.black, fontSize: 14,fontWeight: FontWeight.bold),),
                  ],
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Text(weather.pressure.toString()+" ml/h",style: TextStyle(color: Colors.black, fontSize: 20),),
                    Text("précipitation",style: TextStyle(color: Colors.black, fontSize: 14,fontWeight: FontWeight.bold),),
                  ],
                ),
                Column(
                  children: <Widget>[
                    Text(weather.humidity.round().toString()+" %",style: TextStyle(color: Colors.black, fontSize: 20),),
                    Text("Humidité",style: TextStyle(color: Colors.black, fontSize: 14, fontWeight: FontWeight.bold),),
                  ],
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              width: 200,
              height: 50,
              child: FlatButton(
                shape: new RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10))),
                onPressed: () {
                  BlocProvider.of<WeatherDartBloc>(context).add(ResetDartWeather());
                },
                color: Colors.red,
                child: Text(
                  "Retour",
                  style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),
                ),
              ),
            )
          ],
        )
    );
  }
  Widget getWeatherIcon({required String weatherDescription, required Color color, required double size}){
    switch(weatherDescription){
      case "Clear": {
        return Icon(FontAwesomeIcons.sun,color: color,size: size);
        break;
      }
      case "Clouds": {
        return Icon(FontAwesomeIcons.cloud,color: color,size: size);
        break;
      }
      case "Rain": {
        return Icon(FontAwesomeIcons.cloudRain,color: color,size: size);
        break;
      }
      case "Snow": {
        return Icon(FontAwesomeIcons.snowman,color: color,size: size);
        break;
      }
      default : {
        return Icon(FontAwesomeIcons.sun,color: color,size: size);
        break;
      }
    }
  }
}